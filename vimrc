syntax on

set mouse=a

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set noerrorbells

" Set tabs
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent


set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch

set ruler

" set lazyredraw

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')

" autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" auto closing brackets
Plug 'jiangmiao/auto-pairs'

" read man from vim
Plug 'vim-utils/vim-man'

" watch undotree
Plug 'mbbill/undotree'

" code hightlight
Plug 'sheerun/vim-polyglot'

" filesearch
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" filetree
Plug 'preservim/nerdtree'

" colorcshemes
Plug 'gruvbox-community/gruvbox'
Plug 'sainnhe/gruvbox-material'

call plug#end()

" Backgroung color fix for kitty terminal emulator
let &t_ut=''

let g:gruvbox_contrast_dark = 'medium'
colorscheme gruvbox
set background=dark

let mapleader = " "


" --- vim go (polyglot) settings.
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_format_strings = 1
let g:go_highlight_variable_declarations = 1
let g:go_auto_sameids = 1

" Multiwindows
nmap <leader>vs :vsplit<CR>
nmap <leader>hs :split<CR>
" Multiwindows navigation using hjkl
nnoremap <leader>wj <C-W>j
nnoremap <leader>wk <C-W>k
nnoremap <leader>wl <C-W>l
nnoremap <leader>wh <C-W>h
" Multiwindows cyclic navigation
nnoremap <leader>ww <C-W>w
" Multiwindows navigation using arrow keys
nnoremap <leader>w<Down>  <C-W>j
nnoremap <leader>w<Up>    <C-W>k
nnoremap <leader>w<Right> <C-W>l
nnoremap <leader>w<Left>  <C-W>h

" GoTo code navigation.
nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gy <Plug>(coc-type-definition)
nmap <leader>gi <Plug>(coc-implementation)
nmap <leader>gr <Plug>(coc-references)
nmap <leader>rr <Plug>(coc-rename)
nmap <leader>g[ <Plug>(coc-diagnostic-prev)
nmap <leader>g] <Plug>(coc-diagnostic-next)
nmap <leader>fs <Plug>(coc-format-selected)
nmap <leader>ff <Plug>(coc-format)
nmap <silent> <leader>gp <Plug>(coc-diagnostic-prev-error)
nmap <silent> <leader>gn <Plug>(coc-diagnostic-next-error)
nnoremap <leader>cr :CocRestart

" NerdTree
nnoremap <leader>nn :NERDTreeToggle<CR>
nnoremap <leader>no :NERDTreeToggle 

" Copy to clipboard
" For Mac OS replace 'xsel -b' with 'pbcopy' and 'xsel -o -b' with 'pbpaste'
" Alternative clipboard for Linux replace 'xclip -i -sel c' with 'pbcopy' and 'xclip -o -sel -c' with 'pbpaste'
if has('macunix') 
    vmap <leader>y :w !pbcopy<CR><CR>
    vmap <leader>Y :%w !pbcopy<CR><CR>
    vmap <leader>Y :%w !pbcopy<CR><CR>
    vmap <leader>p :!pbpaste<CR><CR>
    nmap <leader>p :r!pbpaste<CR><CR>
elseif has('win32')
    vmap <leader>y :w !clip<CR><CR>
    vmap <leader>Y :%w !clip<CR><CR>
    vmap <leader>Y :%w !clip<CR><CR>
elseif has('unix')
    vmap <leader>y :w !xsel -b<CR><CR>
    vmap <leader>Y :%w !xsel -b<CR><CR>
    vmap <leader>Y :%w !xsel -b<CR><CR>
    vmap <leader>p :!xsel -o -b<CR><CR>
    nmap <leader>p :r!xsel -o -b<CR><CR>
endif

" Run with python3
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

