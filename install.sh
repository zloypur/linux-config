#!/bin/bash


function ensure_dir_exist {
    mkdir -p $1
}


function copy_config {
    echo "Installing $1 config to '$2'."
    
    if [[ -L "$2" ]]; then
        echo "$1 config is symbolic link. Unlinking..."
        unlink "$2"
    elif [[ -d "$2" ]] || [[ -f "$2" ]]; then
        echo "Found config '$2'. Creating backup..."
        if [[ -e "$2.bak" ]]; then
            rm -r "$2.bak"
        fi
        mv -f "$2" "$2.bak"
        echo "Backup created at '$2.bak'."
    elif [[ -e "$2" ]]; then
        echo "$1 config dir has unsupported type. Skip $1 config install."
    fi

    echo "Copying '$3' to '$2'."

    cp -r "$3" "$2";

    if [[ $? == 0 ]]; then
        echo "Copying succeed."
        return 0
    else
        echo "Copying failed."
        return 1
    fi
}


CONFIG_FOLDER="$HOME/.config"

# Ensure config folred exists
ensure_dir_exist "$CONFIG_FOLDER"

# Installing i3 config
I3_CONFIG_FOLDER="$CONFIG_FOLDER/i3"

copy_config "i3" "$I3_CONFIG_FOLDER" "$PWD/i3"

# Installing ranger config
RANGER_CONFIG_FOLDER="$CONFIG_FOLDER/ranger"

copy_config "ranger" "$RANGER_CONFIG_FOLDER" "$PWD/ranger"

# Installing vim config
VIM_CONFIG_FOLDER="$HOME/.vim"
ensure_dir_exist "$VIM_CONFIG_FOLDER"

copy_config "vim coc settings" "$VIM_CONFIG_FOLDER/coc-settings.json" "$PWD/vim/coc-settings.json"

VIM_CONFIG="$HOME/.vimrc"

copy_config "vimrc" "$HOME/.vimrc" "$PWD/vimrc"

# Installing picom config

copy_config "picom config" "$CONFIG_FOLDER/picom" "$PWD/picom"

echo "Installation complete"

